package com.presenso.testtask.matrixmultiplication;

import com.presenso.testtask.matrix.Matrix;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.stream.IntStream;

import static com.presenso.testtask.matrixmultiplication.TestUtils.assertMatricesEqual;

/**
 * Created by Olena Voloshyna on 30.05.2017.
 */
@RunWith(JUnit4.class)
public class ParallelMatrixMultiplierTest {
    private ParallelMatrixMultiplier matrixMultiplier;

    @Before
    public void setUp()  {
        RowByColumnMultiplier rowByColumnMultiplier = new RowByColumnMultiplier();
        int parallelismLevel = 4;

        matrixMultiplier = new ParallelMatrixMultiplier(rowByColumnMultiplier, parallelismLevel);
    }

    @Test
    public void testMultiply() throws Exception {
        Matrix matrix1 = new TestMatrix(new float[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        Matrix matrix2 = new TestMatrix(new float[][]{
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
        });
        Matrix resultMatrix = new TestMatrix(matrix1.rowsCount(), matrix2.columnsCount());

        matrixMultiplier.multiply(matrix1, matrix2, resultMatrix);

        Matrix expectedResult = new TestMatrix(new float[][]{
                {38, 44, 50, 56},
                {83, 98, 113, 128}
        });
        assertMatricesEqual(expectedResult, resultMatrix);
    }


    @Test
    public void testMultiplyLargeMatrices() throws Exception {
        int size = 1600;

        float[][] sourceArray = new float[size][size];
        float[] sourceArrayRow = new float[size];
        Arrays.fill(sourceArrayRow, 1);
        IntStream.range(0, size).forEach(row -> sourceArray[row] = sourceArrayRow);

        Matrix matrix1 = new TestMatrix(sourceArray);
        Matrix matrix2 = new TestMatrix(sourceArray);
        Matrix resultMatrix = new TestMatrix(size, size);


        float[][] resultArray = new float[size][size];
        float[] resultArrayRow = new float[size];
        Arrays.fill(resultArrayRow, size);
        IntStream.range(0, size).forEach(row -> resultArray[row] = resultArrayRow);
        Matrix expectedResult = new TestMatrix(resultArray);

        long start = System.currentTimeMillis();
        matrixMultiplier.multiply(matrix1, matrix2, resultMatrix);
        System.out.println("Time : " + (System.currentTimeMillis() - start));

        assertMatricesEqual(expectedResult, resultMatrix);
    }
}