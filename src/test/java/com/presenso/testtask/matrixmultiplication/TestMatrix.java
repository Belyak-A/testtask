package com.presenso.testtask.matrixmultiplication;

import com.presenso.testtask.matrix.SimpleMatrixImpl;

/**
 * Created by Olena Voloshyna on 30.05.2017.
 */
class TestMatrix extends SimpleMatrixImpl {
    TestMatrix(int rowsNumber, int columnsNumber) {
        super(rowsNumber, columnsNumber);
    }

    TestMatrix(float[][] values) {
        super(values.length, values[0].length);
        for(int i = 0; i < values.length; i++) {
            for(int j = 0; j < values[i].length; j++) {
                set(i, j, values[i][j]);
            }
        }
    }
}
