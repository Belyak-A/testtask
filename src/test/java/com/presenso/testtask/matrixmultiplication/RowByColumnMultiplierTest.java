package com.presenso.testtask.matrixmultiplication;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RowByColumnMultiplierTest {
    private static final double DELTA = 0.001;
    private RowByColumnMultiplier rowByColumnMultiplier = new RowByColumnMultiplier();

    @Test
    public void testMultiply() {
        Coordinate coordinate = new Coordinate(12, 34);
        float[] array1 = {1, 2, 3};
        float[] array2 = {3, 7, 11};
        CellSourceDataContainer sourceDataContainer = new CellSourceDataContainer(coordinate, array1, array2);

        Pair<Coordinate, Float> cellResult = rowByColumnMultiplier.apply(sourceDataContainer);
        assertEquals((double)50, cellResult.getRight(), DELTA);
    }
}