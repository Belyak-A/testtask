package com.presenso.testtask.matrixmultiplication;

import com.presenso.testtask.matrix.Matrix;

import static org.junit.Assert.assertEquals;

/**
 * Created by Olena Voloshyna on 31.05.2017.
 */
class TestUtils {
    static void assertMatricesEqual(Matrix matrix1, Matrix matrix2) {
        assertEquals(matrix1.rowsCount(), matrix2.rowsCount());
        assertEquals(matrix1.columnsCount(), matrix2.columnsCount());

        int rows = matrix1.rowsCount();
        int cols = matrix1.columnsCount();
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                float m1Value = matrix1.get(i, j);
                float m2Value = matrix2.get(i, j);

                if(Float.compare(m1Value, m2Value) != 0) {
                    throw new AssertionError();
                }
            }
        }
    }
}
