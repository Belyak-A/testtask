package com.presenso.testtask.matrixmultiplication;

import lombok.Data;

@Data
class CellSourceDataContainer {
    private final Coordinate resultCoordinate;
    private final float[] matrix1Row;
    private final float[] matrix2Col;
}
