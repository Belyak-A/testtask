package com.presenso.testtask.matrixmultiplication;


import org.apache.commons.lang3.tuple.Pair;

import java.util.function.Function;

public class RowByColumnMultiplier implements Function<CellSourceDataContainer, Pair<Coordinate, Float>> {
    @Override
    public Pair<Coordinate, Float> apply(CellSourceDataContainer cellSourceDataContainer) {
        Coordinate coordinate = cellSourceDataContainer.getResultCoordinate();

        float[] row = cellSourceDataContainer.getMatrix1Row();
        float[] column = cellSourceDataContainer.getMatrix2Col();

        int size = row.length;
        float result = 0;
        for (int i = 0; i < size; i++) {
            result += row[i] * column[i];
        }
        return Pair.of(coordinate, result);
    }
}
