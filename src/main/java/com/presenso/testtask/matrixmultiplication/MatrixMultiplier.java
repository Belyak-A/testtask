package com.presenso.testtask.matrixmultiplication;

import com.presenso.testtask.matrix.Matrix;

public interface MatrixMultiplier {
    void multiply(Matrix matrix1, Matrix matrix2, Matrix result) throws Exception;
}
