package com.presenso.testtask.matrixmultiplication;

import com.presenso.testtask.matrix.Matrix;

import java.util.function.Function;

class SourceDataSupplier implements Function<Coordinate, CellSourceDataContainer> {
    private final Matrix matrix1;
    private final Matrix matrix2;

    SourceDataSupplier(Matrix matrix1, Matrix matrix2) {
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
    }

    @Override
    public CellSourceDataContainer apply(Coordinate coordinate) {
        float[] rowData = new float[matrix1.columnsCount()];
        for (int col = 0; col < rowData.length; col++) {
            rowData[col] = matrix1.get(coordinate.getX(), col);
        }

        float[] colData = new float[matrix2.rowsCount()];
        for (int row = 0; row < colData.length; row++) {
            colData[row] = matrix2.get(row, coordinate.getY());
        }

        return new CellSourceDataContainer(coordinate, rowData, colData);
    }
}
