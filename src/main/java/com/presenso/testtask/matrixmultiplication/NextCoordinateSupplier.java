package com.presenso.testtask.matrixmultiplication;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.lang3.Validate.notNull;

class NextCoordinateSupplier implements Supplier<Coordinate> {
    private final int rowCount;
    private final int colsCount;
    private AtomicReference<Coordinate> coordinates;

    NextCoordinateSupplier(Coordinate start, int rowCount, int colsCount) {
        notNull(start);
        isTrue(start.getX() < rowCount);
        isTrue(start.getY() < colsCount);

        this.rowCount = rowCount;
        this.colsCount = colsCount;
        this.coordinates = new AtomicReference<>(start);
    }

    @Override
    public Coordinate get() {
        return coordinates.getAndUpdate(prev -> {
            if (prev == null) {
                return null;
            }

            int x = prev.getX();
            int y = prev.getY();

            y++;
            if (y >= colsCount) {
                x += y / colsCount;
                y %= colsCount;
            }
            return x >= rowCount ? null : new Coordinate(x, y);
        });
    }
}
