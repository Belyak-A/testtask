package com.presenso.testtask.matrixmultiplication;

import com.presenso.testtask.matrix.Matrix;

import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.lang3.Validate.notNull;

class ParallelMatrixMultiplier implements MatrixMultiplier {
    private final RowByColumnMultiplier rowByColumnMultiplier;
    private final int parallelismLevel;

    ParallelMatrixMultiplier(RowByColumnMultiplier rowByColumnMultiplier, int parallelismLevel) {
        this.rowByColumnMultiplier = rowByColumnMultiplier;
        this.parallelismLevel = parallelismLevel;
    }

    @Override
    public void multiply(Matrix matrix1, Matrix matrix2, Matrix resultMatrix) throws Exception  {
        notNull(matrix1);
        notNull(matrix2);
        notNull(resultMatrix);
        isTrue(matrix1.rowsCount() == resultMatrix.rowsCount());
        isTrue(matrix2.columnsCount() == resultMatrix.columnsCount());

        NextCoordinateSupplier nextCoordinateSupplier = new NextCoordinateSupplier(new Coordinate(0,0),
                resultMatrix.rowsCount(), resultMatrix.columnsCount());
        SourceDataSupplier sourceDataSupplier = new SourceDataSupplier(matrix1, matrix2);
        ResultsConsumer resultsConsumer = new ResultsConsumer(resultMatrix);

        ForkJoinPool threadPool = new ForkJoinPool(parallelismLevel);
        threadPool.submit(() -> IntStream.range(0, matrix1.rowsCount() * matrix2.columnsCount())
                .parallel()
                .mapToObj(i -> nextCoordinateSupplier.get())
                .map(sourceDataSupplier)
                .map(rowByColumnMultiplier)
                .forEach(resultsConsumer)).get();
    }
}
