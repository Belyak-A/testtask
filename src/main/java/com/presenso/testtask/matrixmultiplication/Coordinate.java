package com.presenso.testtask.matrixmultiplication;

import lombok.Data;

@Data
class Coordinate {
    private final int x;
    private final int y;
}
