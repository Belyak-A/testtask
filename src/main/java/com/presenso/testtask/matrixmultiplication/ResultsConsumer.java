package com.presenso.testtask.matrixmultiplication;

import com.presenso.testtask.matrix.Matrix;
import org.apache.commons.lang3.tuple.Pair;

import java.util.function.Consumer;

class ResultsConsumer implements Consumer<Pair<Coordinate, Float>> {
    private final Matrix resultMatrix;

    ResultsConsumer(Matrix resultMatrix) {
        this.resultMatrix = resultMatrix;
    }

    @Override
    public void accept(Pair<Coordinate, Float> computationResult) {
        Coordinate coordinate = computationResult.getLeft();
        float value = computationResult.getRight();
        resultMatrix.set(coordinate.getX(), coordinate.getY(), value);
    }
}
