package com.presenso.testtask.matrix;

/**
 * Created by Olena Voloshyna on 30.05.2017.
 */
public interface Matrix {
    float get(int row, int column);
    void set(int row, int column, float value);
    int rowsCount();
    int columnsCount();
}
