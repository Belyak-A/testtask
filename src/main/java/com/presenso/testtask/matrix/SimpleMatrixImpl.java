package com.presenso.testtask.matrix;

import lombok.ToString;

/**
 * Created by Olena Voloshyna on 30.05.2017.
 */
@ToString(of = "values")
public class SimpleMatrixImpl implements Matrix {
    private final int rowsNumber;
    private final int columnsNumber;
    private final float[][] values;

    public SimpleMatrixImpl(int rowsNumber, int columnsNumber) {
        this.rowsNumber = rowsNumber;
        this.columnsNumber = columnsNumber;
        this.values = new float[rowsNumber][columnsNumber];
    }

    public float get(int row, int column) {
        return values[row][column];
    }

    public void set(int row, int column, float value) {
        values[row][column] = value;
    }

    public int rowsCount() {
        return rowsNumber;
    }

    public int columnsCount() {
        return columnsNumber;
    }
}
